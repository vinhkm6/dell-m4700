# Dell M4700 Hackintosh

## System specs:

- CPU: Intel Core i7 - 3720QM
- iGPU: Intel HD4000
- GPU: NVIDIA Quadro K1000M
- Screen: 1920x1080 60Hz
- Audio: IDT92HD93BXX
- Wifi: Replaced to BCM943224 HMS
- Bluetooth: Built-in
- Camera: Built-in

## Patchs applied:

### DSDT/SSDT:
- 3 PARSEOP_* errors - [Thread link](https://www.tonymacx86.com/threads/solved-help-me-fix-dsdt-error.229025/)
- A disable discrete patch but it's not work.
- SSDT-PNLF.aml 

### Via Clover

#### ACPI
- Disable NVDIA Quadro K1000M by go to Clover configurator -> Acpi -> uncheck NVidia (I'm not sure, it's not working now!)

#### Kernel and kext patchs:

- USB 10.13.4+ by PMHeart

### Kexts:
- Enable wifi (Lilu + AirportBrcmFixup)
- AppleBacklightFixUp (SSDT-PNLF required) - [Link](https://bitbucket.org/RehabMan/applebacklightfixup/overview)

## Patchs not applied:

- Camera (not working at 10.13.4)
- Sleep when close the lid.
- FaceTime + iMessages.
- Vice versa ...

